package com.gu.management.servlet

/*
 * Copyright 2008-2011 WorldWide Conferencing, LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.net.URL
import java.util
import java.util.{ Enumeration => JEnum }
import java.util.{ HashMap => JHash }

import scala.collection.JavaConverters._

/**
 * A Mock ServletRequest. Change its state to create the request you are
 * interested in. At the very least, you will need to change method and path.
 *
 * There are several things that aren't supported:
 *
 * <ul>
 *   <li>getRequestDispatcher - returns null always</li>
 *   <li>getRequestedSessionId - always returns null. The related
 *       isRequestedSessionId... methods similarly all return false</li>
 *   <li>getRealPath - simply returns the input string</li>
 * </ul>
 *
 * @author Steve Jenson (stevej@pobox.com)
 * @author Derek Chen-Becker
 *
 * @param url The URL to extract from
 * @param contextPath The context path for this request. Defaults to "" per the Servlet API.
 *
 */
class MockHttpServletRequest(override val url: String = null, contextPath: String = "") extends MockHttpServletRequestBase(url, contextPath) {

  /**
   * Construct a new mock request for the given URL. See processUrl
   * for limitations.
   *
   * @param url The URL to extract from
   */
  def this(url: URL) = {
    this()
    processUrl(url)
  }

  /**
   * Construct a new mock request for the given URL. See processUrl
   * for limitations.
   *
   * @param url The URL to extract from
   * @param contextPath The servlet context of the request.
   */
  def this(url: URL, contextPath: String) = {
    this(null: String, contextPath)
    processUrl(url)
  }

  override def getAttributeNames(): JEnum[Object] = attributes.keys.iterator.asInstanceOf[Iterator[Object]].asJavaEnumeration

  override def getLocales(): JEnum[Object] = locales.iterator.asInstanceOf[Iterator[Object]].asJavaEnumeration

  override def getParameterMap(): util.Map[Object, Object] = {
    // Build a new map based on the parameters List
    var newMap = Map[String, List[String]]().withDefault(ignore => Nil)

    parameters.foreach {
      case (k, v) => newMap += k -> (newMap(k) ::: v :: Nil) // Ugly, but it works and keeps order
    }

    newMap.map { case (k, v) => (k, v.toArray) }.asInstanceOf[Map[Object, Object]]
  }.asJava

  override def getParameterNames(): JEnum[Object] = parameters.map(_._1).distinct.iterator.asInstanceOf[Iterator[Object]]
    .asJavaEnumeration

  override def getHeaderNames(): JEnum[Object] = headers.keys.iterator.asInstanceOf[Iterator[Object]].asJavaEnumeration

  override def getHeaders(s: String): JEnum[Object] = headers.getOrElse(s, Nil).iterator.asInstanceOf[Iterator[Object]]
    .asJavaEnumeration
}
