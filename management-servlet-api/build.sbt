
name := "management-servlet-api"

licenses +=("Apache-2.0", url("http://www.apache.org/licenses/LICENSE-2.0"))

libraryDependencies ++= Seq(
    "javax.servlet" % "servlet-api" % "2.4" % "provided",
    "com.github.pathikrit" %% "better-files-akka" % "3.9.1",
    "commons-codec" % "commons-codec" % "1.6" % "test",
    "org.specs2" %% "specs2-core" % "4.10.3" % "test",
    "org.scalatest" %% "scalatest" % "3.2.3" % "test",
    "org.mockito" % "mockito-core" % "3.6.28" % "test"
)

enablePlugins(JettyPlugin)

resolvers += "Scalaz Bintray Repo" at "https://dl.bintray.com/scalaz/releases"

// disable publishing the main javadoc jar
publishArtifact in (Compile, packageDoc) := false

publishTo := Some("uxforms-public" at "s3://artifacts-public.uxforms.net")
