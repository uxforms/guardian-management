import java.util.jar._

import com.typesafe.sbt.SbtScalariform
import sbt.Def

organization in ThisBuild := "com.gu"

scalaVersion in ThisBuild := "2.11.2"

crossScalaVersions in ThisBuild := Seq("2.11.2", "2.13.4")

publishArtifact := false

Compile / packageBin / packageOptions +=
  Package.ManifestAttributes(
    java.util.jar.Attributes.Name.IMPLEMENTATION_VERSION -> version.value,
    java.util.jar.Attributes.Name.IMPLEMENTATION_TITLE -> name.value,
    java.util.jar.Attributes.Name.IMPLEMENTATION_VENDOR -> "guardian.co.uk"
  )

scalacOptions in ThisBuild += "-deprecation"

publishTo := Some("uxforms-public" at "s3://artifacts-public.uxforms.net")


val noPublish: Seq[Def.Setting[Task[Unit]]] = Seq(
  publish := {},
  publishLocal := {})


lazy val root = (project in file(".")).aggregate(
  management,
  managementServletApi,
  managementInternal,
  managementLogback,
  exampleServletApi
).settings(noPublish)

lazy val management = (project in file("management")).settings(SbtScalariform.scalariformSettings: _*)
lazy val managementServletApi = ((project in file("management-servlet-api")).settings(SbtScalariform.scalariformSettings: _*) dependsOn management)
lazy val managementInternal = (project in file("management-internal")).settings(SbtScalariform.scalariformSettings: _*) dependsOn management
lazy val managementLogback = (project in file("management-logback")).settings(SbtScalariform.scalariformSettings: _*) dependsOn management

lazy val exampleServletApi = (project in file("example-servlet-api")).settings(SbtScalariform.scalariformSettings: _*).dependsOn(
  management,
  managementServletApi,
  managementLogback
).settings(noPublish)

resolvers += Resolver.typesafeRepo("releases")
