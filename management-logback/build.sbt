
name := "management-logback"

licenses +=("Apache-2.0", url("http://www.apache.org/licenses/LICENSE-2.0"))

libraryDependencies ++= Seq(
    "ch.qos.logback" % "logback-classic" % "0.9.27",
    "javax.servlet" % "servlet-api" % "2.4" % "provided"
)

// disable publishing the main javadoc jar
publishArtifact in (Compile, packageDoc) := false

publishTo := Some("uxforms-public" at "s3://artifacts-public.uxforms.net")
