
licenses +=("Apache-2.0", url("http://www.apache.org/licenses/LICENSE-2.0"))

libraryDependencies ++= Seq(
    "org.slf4j" % "slf4j-api" % "1.6.1",
    "org.scala-lang.modules" %% "scala-xml" % "1.3.0",
    "org.json4s" %% "json4s-jackson" % "3.6.10",
    "org.specs2" %% "specs2-core" % "4.10.0" % Test
)

resolvers += Resolver.typesafeRepo("releases")

resolvers += "Scalaz Bintray Repo" at "https://dl.bintray.com/scalaz/releases"

// disable publishing the main javadoc jar
publishArtifact in (Compile, packageDoc) := false

publishTo := Some("uxforms-public" at "s3://artifacts-public.uxforms.net")
