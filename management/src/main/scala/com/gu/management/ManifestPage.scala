package com.gu.management

import io.Source
import scala.language.postfixOps

class ManifestPage extends ManagementPage {
  val path = "/management/manifest"

  def get(req: HttpRequest) = response

  lazy val response = PlainTextResponse(
    Manifest.asStringOpt.getOrElse("Could not find version.txt on classpath.  Did you include the sbt-version-info-plugin?"))
}

object Manifest {

  lazy val asStringOpt: Option[String] =
    Option(getClass.getResourceAsStream("/version.txt")) map (Source fromInputStream _ mkString)
  lazy val asString: String = asStringOpt getOrElse ""
  lazy val asList: List[String] = asStringOpt map { _.split("\n").toList } getOrElse Nil
  lazy val asKeyValuePairs: Map[String, String] = (asList map { _.split(":") } collect { case Array(k, v) => k -> v }).toMap
}
