
name := "management-internal"

licenses +=("Apache-2.0", url("http://www.apache.org/licenses/LICENSE-2.0"))

resolvers ++= Seq(Classpaths.typesafeReleases)

resolvers += "Scalaz Bintray Repo" at "https://dl.bintray.com/scalaz/releases"

libraryDependencies ++= Seq(
  "org.specs2" %% "specs2-core" % "4.10.0" % Test,
  "com.github.pathikrit" %% "better-files" % "3.9.1"
)

// disable publishing the main javadoc jar
publishArtifact in (Compile, packageDoc) := false

publishTo := Some("uxforms-public" at "s3://artifacts-public.uxforms.net")
