//resolvers ++= Seq(
////  Classpaths.typesafeResolver,
// // "Web plugin repo" at "http://siasia.github.com/maven2"
//)

addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.8.6")

addSbtPlugin("org.scalariform" % "sbt-scalariform" % "1.8.3")

addSbtPlugin("com.github.gseitz" % "sbt-release" % "1.0.13")

addSbtPlugin("com.earldouglas" % "xsbt-web-plugin" % "4.2.1")

addSbtPlugin("com.frugalmechanic" % "fm-sbt-s3-resolver" % "0.18.0")
